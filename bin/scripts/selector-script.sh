# en $1 it comes the filename of the txt to be anonymized
# selector is the config file
for i in `cat $1`
do
rex=$(awk -F ';;;' '{print $1}' <<< "$i")
fname=$(awk -F ';;;' '{print $2}' <<< "$i")
if [[ $22 =~ $rex ]];
then
    echo $fname
    exit 0
fi
done

