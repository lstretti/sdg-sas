#!/usr/bin/bash
echo "anonymize process starts at: "`date` >> /var/log/sdg.log
s="/var/www/tiny/data"
anonymized_dir="$s/anonymized-data"
rm -rf $s/anonymized-data/
mkdir -p $s/anonymized-data
cp -rf $s/intermediate-data/decoded/* $anonymized_dir/
for i in `find $anonymized_dir`
do
  if [[ -f $i ]]
  then
    echo "sobre fichero: "$i
    echo "un list:"
    ls -la $i
    yamlf=$(/home/savana/bin/scripts/selector-script.sh $s/intermediate-data/conf/templates/selector.txt $i)
    echo "yamlf del selector: "$yamlf
    if [[ "$yamlf" == '' ]]; then
      continue
    fi
    /home/savana/bin/scripts/xtr.py $i $s/intermediate-data/conf/templates/$yamlf
    /home/savana/bin/scripts/cnv.py $i /tmp/output.yaml
    rm $i
    j="$(basename -- $i)"
    cp /tmp/final.txt $i
  fi
done
echo "scan after anonymize start at: "`date` >> /var/log/sdg.log
#sudo -u www-data php /var/www/nextcloud/occ files:scan --all
echo "anonymize process stops at: "`date` >> /var/log/sdg.log
