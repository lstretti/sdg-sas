#!/usr/bin/python3
import re, sys, yaml, os
# import stanza
debug = 0
print("start load")
if debug == 1:
  # text_file = open("../test/testfile.txt", "r", encoding="ISO-8859-1")
  text_file = open("../test/testfile.txt", "r", encoding="UTF-8")
else:
  text_file = open(sys.argv[1], "r", encoding="UTF-8")
co = text_file.read()
text_file.close()
print("end load")

if debug == 1:
  with open("../test/testtemplate.yaml") as file:
    patterns = yaml.load(file, Loader=yaml.FullLoader)
else:
  with open(sys.argv[2]) as file:
    patterns = yaml.load(file, Loader=yaml.FullLoader)
print(patterns)
out = {}

for ptn in patterns:
  # print(ptn)
  # print(ptn['type'])
  name = ptn['name']
  if ptn['type'] == 'REX':
    p = re.compile(ptn["pattern"])
    if name not in out:
      out[name] = []
      out[name].append(ptn['ano'])
    for m in p.finditer(co):
      out[name].append([m.start(1), m.group(1)])
  elif ptn['type'] == 'CSV1':
    print("start CSV")
    ncol = int(ptn['col']) - 1
    sep = ptn['separator']
    hea = ptn['header']
    print("col {}".format(ncol))
    if name not in out:
      out[name] = []
      out[name].append(ptn['ano'])
    gpos = 0
    ln = co.split("\n")
    # next if is WRONG put in right place
    # if hea == "yes":
    #   del ln[0]
    lp = 0
    for field in ln:
      print("gpos {}".format(gpos))
      col = field.split(sep)
      print(col)
      print("len: {}, ncol {}".format(len(col), ncol))
      for z in range(0, ncol):
        print("len de col[z]: {}".format(len(col[z])))
        lp = gpos + len(col[z]) + 1
        print("LPOS: {}".format(lp))
      if len(col) > ncol:
        fi = col[ncol]
        out[name].append([lp, fi])
        print("first 3 letters {}".format(co[lp:lp + 5]))
      gpos += len(field) + 1
  elif ptn['type'] == 'CSV':
    if name not in out:
      out[name] = []
      out[name].append(ptn['ano'])
    ncol = int(ptn['col']) - 1
    sep = ptn['separator']
    hea = ptn['header']
    col = 0
    col_before = 0
    xout = []
    for i in range(0, len(co)):
      if co[i] != "\n" and co[i] != sep:
        continue
      elif co[i] == "\n":
        if col == ncol:
          xout.append([col_before, co[col_before:i]])
        col = 0
        col_before = i + 1
        continue
      elif co[i] == sep:
        if col == ncol:
          xout.append([col_before, co[col_before:i]])
        col_before = i
        col += 1
        continue
    # print(xout)
    for e in xout:
      out[name].append(e)
    print(out)
  else:
    # the inputs are co which is the full text
    # and the ptr which is the dict with 'name', 'type', 'ano',..and the specific fields use by this pattern
    # the output is xout which is [start-point-co, contents],[start2, content2],....]
    if debug == 1:
      plugin = os.listdir('../test/plugins/')
    else:
      plugin = os.listdir('/var/www/tiny/data/intermediate-data/conf/plugins')
    if ptn['type'] in plugin:
      xout = []
      if debug == 1:
        exec(open("../test/plugins/" + ptn['type']).read())
      else:
        exec(open('/var/www/tiny/data/intermediate-data/conf/plugins/'+ptn['type']).read())
      print("OUT: ")
      print(type(out))
      print(out)
      if name not in out:
        out[name] = []
        print(out)
        print(ptn['ano'])
        out[name].append(ptn['ano'])
      for e in xout:
        out[name].append(e)
print(out)
with open('/tmp/output.yaml', 'w') as f:
  d = yaml.dump(out, f)

