echo "starts upload to Savana at: "`date` >> /var/log/sdg.log
user=`aws sts get-caller-identity|grep Arn|cut -d":" -f7|cut -d"/" -f2|cut -d'"' -f1`
aws s3 sync /var/www/tiny/anonymized-data s3://$user/upload
echo "End of the upload to Savana at: "`date` >> /var/log/sdg.log
