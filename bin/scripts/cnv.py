#!/usr/bin/python3
import re, sys, yaml
# import stanza

debug = 0
if debug == 1:
  text_file = open("../test/testfile.txt", "r", encoding="UTF-8")
else:
  text_file = open(sys.argv[1], "r", encoding="UTF-8")
co = text_file.read()
text_file.close()

if debug == 1:
  with open("/tmp/output.yaml") as f:
    patterns = yaml.load(f, Loader=yaml.FullLoader)
else:
  with open(sys.argv[2]) as f:
    patterns = yaml.load(f, Loader=yaml.FullLoader)
  # print(patterns)

lchanges = []
for z in patterns:
  for i in range(1, len(patterns[z])):
    print(patterns[z][i])
    lchanges.append([patterns[z][0], patterns[z][i][0], patterns[z][i][1]])
lchanges = sorted(lchanges, key=lambda x: x[1])
# print(lchanges)
stops = []
stops_cont = []
pieces = []
for k in lchanges:
  stops.append(k[1])
for k in lchanges:
  stops_cont.append(k[2])
ini = 0
for k in range(0, len(stops)):
  s = stops[k]
  a = co[ini:s]
  pieces.append(a)
  b = co[s:s + len(stops_cont[k])]
  pieces.append(b)
  ini = s + len(stops_cont[k])
if len(stops) == 0:
  pieces.append(co)
elif len(co) > s + len(stops_cont[k]):
  pieces.append(co[ini:len(co)])
print(pieces)

outp = ""
print(pieces)
for k in range(0, len(lchanges)):
  outp += pieces[k * 2]
  if lchanges[k][0] == "ofus":
    outp += "X" * len(pieces[(k * 2) + 1])
  else:
    outp += "YY-" + str(hash(pieces[(k * 2) + 1]))
outp += pieces[len(pieces) - 1]
print(outp)
text_file = open("/tmp/final.txt", "w")
n = text_file.write(outp)
text_file.close()

