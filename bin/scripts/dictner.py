#!/usr/bin/python3

import re, sys, unidecode
tits = {"D":"", "Dña": "", "Dr":"", "Dra":"", "Sr":"", "Sra":""}
na = open("datasets/es_names.txt", "r", encoding="UTF-8").read()
nams = {}
for x in na.split("\n"):
  nams.update({x:"N"})
na = open("datasets/es_surnames.txt", "r", encoding="UTF-8").read()
for x in na.split("\n"):
  nams.update({x:"S"})
debug = 1
if debug == 1:
  # text_file = open("../test/testfile.txt", "r", encoding="ISO-8859-1")
  text_file = open("../test/testfile.txt", "r", encoding="UTF-8")
else:
  text_file = open(sys.argv[1], "r", encoding="UTF-8")
co = text_file.read()
text_file.close()
out = []
ptn = '(\w+)'
p = re.compile(ptn)
for m in p.finditer(co):
    out.append([m.start(1), m.group(1)])
words = []
for x in out:
  words.append(x[1])
ort = []
# para catalan hay que meter è por ejemplo es un poco excesivo
p = re.compile('^[A-ZÁÉÍÓÚÑ]{1}[A-ZÁÉÍÓÚÑa-záéíóúñ]+$')
for i in words:
  if re.search(p, i):
    ort.append(1)
  else:
    ort.append(0)
tit = []
for i in words:
  if i in tits:
    tit.append(1)
  else:
    tit.append(0)
nam = []
for i in words:
  u = i.upper()
  u = u.replace("Á","A")
  u = u.replace("É","A")
  u = u.replace("Í","A")
  u = u.replace("Ó","A")
  u = u.replace("Ú","A")
  u = u.replace("Ü","U")
  # u = unidecode.unidecode(i.upper())
  #u = unidecode.unidecode(u)
  if u in nams:
    nam.append(1)
  else:
    nam.append(0)
antti = [0]
for x in range(1, len(words)):
  if tit[x - 1] == 1:
    antti.append(1)
  else:
    antti.append(0)
antna = [0]
for x in range(1, len(words)):
  if nam[x - 1] == 1:
    antna.append(1)
  else:
    antna.append(0)
posna = []
for x in range(0, len(words)-1):
  if nam[x + 1] == 1:
    posna.append(1)
  else:
    posna.append(0)
posna.append(0)
leng = []
for x in range(0, len(words)):
  if len(words[x]) > 3:
    leng.append(1)
  else:
    leng.append(0)
su = []
for x in range(0, len(words)):
  # print("--->> {}".format(words[x]))
  if ort[x] == 0:
    su.append(0)
    continue
  elif nam[x] == 1:
    if antti[x] == 1 or antna[x] == 1 or posna[x] == 1:
      su.append(1)
      continue
  elif nam[x] == 0:
    if antti[x] == 1:
      su.append(1)
      continue
  su.append(0)
for x in range(0, len(words)):
 print("word: {}, nam: {}, ort: {}, antti: {}, antna: {}, posna: {}, leng: {} su: {}".format(words[x], nam[x], ort[x], antti[x], antna[x], posna[x], leng[x], su[x]))
for x in range(0, len(words)):
  if su[x] == 1:
    print(words[x])
xout = []
for x in range(0, len(words)):
  if su[x] == 1:
    xout.append(out[x])
print(xout)

