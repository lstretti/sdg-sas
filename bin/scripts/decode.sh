#!/usr/bin/bash
echo "decode process starts at: "`date` >> /var/log/sdg.log
s="/var/www/tiny/data"
rm -rf $s/intermediate-data/decoded/
mkdir -p $s/intermediate-data/decoded
cp -rf $s/intermediate-data/uncompressed/* $s/intermediate-data/decoded/
decoded_dir="$s/intermediate-data/decoded"
for i in `find $decoded_dir`;
do
echo $i
ext=`basename -- $i|cut -d'.' -f2`
if [ $ext == "pdf" ] || [ $ext == "PDF" ]; then
    pdftotext $i $i.txt
    rm $i
elif [ $ext == "rtf" ] || [ $ext == "RTF" ]; then
    unoconv -f txt $i 
#    /home/savana/bin/scripts/rtf2txt.py $i > $i.txt
    rm $i
elif [ $ext == "" ]; then
    echo "SUBDirectory"
elif [ $ext == "xhtml" ]; then
    #html2text -ascii  $i > $i.txt 
    #sed -e 's/<[^>]*>//g' $i
    lynx -dump $i > $i.txt
    rm $i
    #mv $i $i.txt
fi
done
for i in `find $decoded_dir`:
do
  encoding=`file -i $i | cut -f 2 -d";" | cut -f 2 -d=`
  case $encoding in
  iso-8859-1)
  iconv -f  ISO-8859-1 -t UTF-8 $i > $i.utf8.txt
  rm $i
  ;;
  esac
done

chmod -R o+r $decoded_dir
chown -R www-data:www-data $decoded_dir 
echo "scan process starts at: "`date` >> /var/log/sdg.log
# sudo -u www-data php /var/www/nextcloud/occ files:scan --path="/savana/files/intermediate-data/decoded"
echo "scan process ends at: "`date` >> /var/log/sdg.log

