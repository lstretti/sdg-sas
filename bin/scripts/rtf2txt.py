#!/usr/bin/python3
from striprtf.striprtf import rtf_to_text
import sys
import codecs

#co = codecs.open(sys.argv[1], "r",  encoding="ISO-8859-1").read()
co = open(sys.argv[1], "r").read()
text = rtf_to_text(co)
print(text)
