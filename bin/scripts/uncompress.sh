#!/usr/bin/bash
echo "uncompress process starts at: "`date` >> /var/log/sdg.log
echo "user name: "`whoami` >> /var/log/sdg.log
s="/var/www/tiny/data"
rm -rf $s/intermediate-data/uncompressed/
mkdir -p $s/intermediate-data/uncompressed
cp -rf $s/source-data/* $s/intermediate-data/uncompressed/
uncompressed_dir="$s/intermediate-data/uncompressed"
for i in `find $uncompressed_dir`;
do
case "$i" in 
	*.tar.bz2)
		echo "fichero entero: "$i
		j="$(basename -- $i)"
		echo "basename: "$j
		f="${j//./_}"
   		x=$(dirname "$i") 
		echo "dirname: "$x
	        echo "mkdir from script: $x/$f" 	
	        mkdir -p $x/$f
   		tar -xf $i -C $x/$f/ 
   		rm $i ;;
   *.gz)
		echo "fichero entero: "$i
		j="$(basename -- $i)"
		echo "basename: "$j
		f="${j//./_}"
   		x=$(dirname "$i")
		echo "dirname: "$x
	        echo "mkdir from script: $x/$f"
	    #    mkdir -p $x/$f
	    #    tar xvzf
	    gunzip  $i
   		# gzip -d $i -C $x/$f/
   		rm $i ;;
   *.zip)
   	echo "fichero entero: "$i
		j="$(basename -- $i)"
		echo "basename: "$j
		f="${j//./_}"
   		x=$(dirname "$i")
		echo "dirname: "$x
	  echo "mkdir from script: $x/$f"
	  mkdir -p $x/$f
	  unzip $i -d $x/$f
 		rm $i ;;
	*)
		echo "no expected file extension" ;;
esac
done
chmod -R o+r $uncompressed_dir  
chown -R www-data:www-data $uncompressed_dir
echo "scan process starts at: "`date` >> /var/log/sdg.log
# sudo -u www-data php /var/www/nextcloud/occ files:scan --path="/savana/files/intermediate-data/uncompressed"
echo "scan process ends at: "`date` >> /var/log/sdg.log
