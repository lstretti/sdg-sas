from flask import Flask , render_template
import os
import subprocess as sp

directory = os.getcwd() 
app = Flask(__name__)
 
@app.route('/')
def sdg():
    return render_template("sdg.html")

@app.route('/anonymize')
def anonymize():
    os.system("nohup /home/savana/bin/scripts/anonymize.sh &")
    return render_template("anonymize.html")
 
@app.route('/upload')
def upload():
    os.system("nohup /home/savana/bin/scripts/upload-savana.sh &")
    return render_template("upload.html")

@app.route('/help')
def help():
    return render_template("help.html")

@app.route('/logs')
def logs():
    logs = sp.getoutput('tail -20 /var/log/sdg.log')
    logs = logs.replace('\n', '<br>')
    return render_template("logs.html", logs=logs)

app.run(host='0.0.0.0', port=8080)
